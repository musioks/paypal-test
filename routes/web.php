<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/about', function () {
    return view('about');
});


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
Route::get('/', 'Customer\PaymentController@index');

// route for processing payment
Route::post('/', 'Customer\PaymentController@payWithpaypal');

// route for check status of the payment
Route::get('status', 'Customer\PaymentController@getPaymentStatus');
