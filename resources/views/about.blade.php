@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">About Us: Testing Vue js</div>

                    <div class="card-body">
                        <div id="app">
                            {{--<ul >--}}
                            {{--<li v-for="user in users">@{{user.name}}</li>--}}
                            {{--</ul>--}}
                            <select class="form-control">
                                <option v-for="user in users" :value="stateAge(user.age)">@{{getFullName(user.firstName,
                                    user.lastName)}}
                                </option>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        let app = new Vue({
            el: '#app',
            data: {
                users: [
                    {firstName: 'john', lastName: 'mwandikwa', age: 50},
                    {firstName: 'janet', lastName: 'jackson', age: 40},
                    {firstName: 'paul', lastName: 'mwikya', age: 20},
                ]
            },
            methods: {
                getFullName: function (first, last) {
                    return first + ' ' + last;
                },
                stateAge: function (age) {
                    return age < 30 ? 'Young' : 'Very Old';
                }
            }

        })
    </script>
@endsection
